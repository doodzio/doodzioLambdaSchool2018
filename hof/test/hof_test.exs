defmodule HofTest do
  use ExUnit.Case
  doctest Hof

  test "1st fibonacci " do
    assert Me.fibonacci(0) == 0
  end
  test "2nd fibonacci " do
    assert Me.fibonacci(1) == 1
  end
  test "3rd fibonacci " do
    assert Me.fibonacci(2) == 1
  end
  test "4th fibonacci " do
    assert Me.fibonacci(3) == 2
  end
  test "10th fibonacci " do
    assert Me.fibonacci(9) == 34
  end

end
