defmodule Hof do
  @moduledoc """
  Documentation for Hof.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Hof.hello
      :world

  """
  def hello do
    :world
  end

  #repeat function
  # repeat(a :: list(), f :: function(), t :: integer()) :: list()
  # t times repeat function f over arguments a
  def repeat(a,_,0) do
    a
  end

  def repeat(a, f , t) do
    cond do
      t < 0 ->
        raise "value < 0 "
      true ->
      repeat(apply(f, a),f,t-1)
    end
  end
end



